# RT Default
resource "aws_default_route_table" "rt-default" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.natgw.id
  }

  lifecycle {
    ignore_changes = all
  }

  tags = {
    Name = "${var.project_name}_rt-default"
  }

}

# RT Public
resource "aws_route_table" "rt-public" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${var.project_name}_rt-public"
  }
}

# RT Associations
resource "aws_route_table_association" "rt-public-assn" {
  subnet_id      = aws_subnet.sbnt_public1.id
  route_table_id = aws_route_table.rt-public.id
}

resource "aws_route_table_association" "rt-private-assn1" {
  subnet_id      = aws_subnet.sbnt_private1.id
  route_table_id = aws_default_route_table.rt-default.id
}
