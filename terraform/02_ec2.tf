# EC2
resource "aws_instance" "master" {
  count           = 1
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-m.sh")

  tags = {
    Name = "master-${count.index + 1}"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}

# Extra EBS Volumes
resource "aws_ebs_volume" "master-vol" {
  count             = 1
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "master${count.index + 1}-vol-1"
  }
}

# EBS Attachments
resource "aws_volume_attachment" "master-vol-attach" {
  count       = 1
  device_name = "/dev/sdb"
  volume_id   = aws_ebs_volume.master-vol.*.id[count.index]
  instance_id = element(aws_instance.master.*.id, count.index)
}


resource "aws_instance" "worker" {
  count           = 3
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-w.sh")

  tags = {
    Name = "worker-${count.index + 1}"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}
